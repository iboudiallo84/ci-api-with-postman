<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210125015108 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TEMPORARY TABLE __temp__demo AS SELECT id, number, description, created_at FROM demo');
        $this->addSql('DROP TABLE demo');
        $this->addSql('CREATE TABLE demo (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, number INTEGER NOT NULL, description CLOB NOT NULL COLLATE BINARY, created_at DATETIME DEFAULT NULL)');
        $this->addSql('INSERT INTO demo (id, number, description, created_at) SELECT id, number, description, created_at FROM __temp__demo');
        $this->addSql('DROP TABLE __temp__demo');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TEMPORARY TABLE __temp__demo AS SELECT id, number, description, created_at FROM demo');
        $this->addSql('DROP TABLE demo');
        $this->addSql('CREATE TABLE demo (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, number INTEGER NOT NULL, description CLOB NOT NULL, created_at DATETIME NOT NULL)');
        $this->addSql('INSERT INTO demo (id, number, description, created_at) SELECT id, number, description, created_at FROM __temp__demo');
        $this->addSql('DROP TABLE __temp__demo');
    }
}
