//https://circleci.com/blog/testing-an-api-with-postman/?utm_medium=SEM&utm_source=gnb&utm_campaign=SEM-gb-DSA-Eng-emea&utm_content=&utm_term=dynamicSearch-&gclid=Cj0KCQiA0rSABhDlARIsAJtjfCfxHkWCU9hk0kC2-Nv0DdNnddJDnNS81snOMwN0nTGAENwlfrsRCZsaAse7EALw_wcB
//For {{url}}/api/users in post in create
//Should create a user
/**
 * {
    "email": "{{$randomEmail}}",
    "password": "password"
}
*/
pm.environment.set("username", pm.response.json().username); 
pm.test("Status code is 200", function () {
    pm.response.to.have.status(201);
});

//Should NOT create a user ==> status 500
/**
 * {
    "email": "{{username}}",
    "password": "password"
}
 */
pm.test("Status code is 500, can not create an other user with an existing email", function () {
    pm.response.to.have.status(500);
});

// login_check
pm.environment.set('token', pm.response.json().token);
pm.test("Status test get token", function () {
    pm.response.to.have.status(200);
});
